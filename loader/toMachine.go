// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package loader

import (
	"gitlab.com/StephaneBunel/omap"
	"gitlab.com/StephaneBunel/vfsm/machine"
)

// ToMachine turns the current intermediate representation into a new Machine
func (ldr *Loader) ToMachine() (*machine.Machine, error) {
	if ldr.specLoaded == false {
		return nil, ErrSpecNotLoaded
	}

	m := machine.New()

	// State entry & exit
	for iter := ldr.IR.States.IterItems(); iter.Next(); {
		stateID, item := iter.Item()
		state := item.Value().(State)
		entry := state.Entry
		if entry == nil {
			entry = []int{}
		}
		exit := state.Exit
		if exit == nil {
			exit = []int{}
		}
		m.StateToEntryMap[stateID] = entry
		m.StateToExistMap[stateID] = exit
	}

	// Rules
	for iter := ldr.IR.Rules.IterItems(); iter.Next(); {
		_, item := iter.Item()
		rule := item.Value().(Rule)
		if _, exists := m.StateToRulesMap[rule.FromState]; !exists {
			m.StateToRulesMap[rule.FromState] = make([]machine.Rule, 0)
		}
		m.StateToRulesMap[rule.FromState] = append(m.StateToRulesMap[rule.FromState],
			machine.Rule{
				ToState:    rule.ToState,
				WhenEvents: rule.OnEvents,
			})
	}

	// Stop states
	for k := range ldr.IR.stateStops {
		m.StateStopMap[k] = struct{}{}
	}

	// Start state
	m.StateStart = ldr.IR.stateStart

	// PathLimit
	m.PathLimit = ldr.IR.pathLimit

	// Cycle Limit
	m.CycleLimit = ldr.IR.cyclesLimit

	// Options
	ldr.IR.Options.VisitItems(func(item omap.Item) {
		switch item.Key() {
		case "--debug":
			m.Options |= machine.OptionsDebug
		case "--record-path":
			m.Options |= machine.OptionsRecordPath
		}
	})

	return m, nil
}
