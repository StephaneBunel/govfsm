// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package loader_test

import (
	"testing"

	"gitlab.com/StephaneBunel/omap"
	"gitlab.com/StephaneBunel/vfsm/loader"
	"gitlab.com/StephaneBunel/vfsm/test"
)

func Test_ToMachineFail(t *testing.T) {
	ldr := loader.New()

	m, err := ldr.ToMachine()
	if m != nil {
		t.Errorf("want %v, got %v", nil, m)
	}
	if err != loader.ErrSpecNotLoaded {
		t.Errorf("want %v, got %v", loader.ErrSpecNotLoaded, err)
	}
}

func Test_ToMachine(t *testing.T) {
	ldr := loader.New()
	if err := ldr.LoadYAML(test.YamlFormalSpecTest); err != nil {
		t.Errorf("want no error, got %v", err)
	}
	ldr.IR.Options.SetSilently(omap.NewItem("--debug", nil), omap.NewItem("--record-path", nil))

	m, err := ldr.ToMachine()
	if err != nil {
		t.Errorf("want %v, got %v", nil, err)
	}
	if m == nil {
		t.Errorf("want %v, got %v", "not nil", m)
	}
}
