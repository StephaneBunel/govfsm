// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

/*
This package contains a Vfsm formal descriptions loader and checker.

For the computer a Vfsm is described by a bunch of data structure using very basic data types.
A formal description use higher level language (more human friendly) to describe a Vfsm.
A loader reads a formal description, check it's consistency, and translate it into a Machine object usable by a Vfsm.
To do that a loader convert the formal description into an intermediate representation of the future Machine.
The use of an intermediate representation allows to easily implements multiple loaders.

The first loader implemented use YAML as description language.
*/
package loader

import "fmt"

// New returns a formal specifications loader
func New() *Loader {
	ldr := new(Loader)
	return ldr
}

// Loader object
type Loader struct {
	IR         LIR
	lastError  error
	specLoaded bool
}

// State object
type State struct {
	Name  string
	Desc  string
	Entry []int
	Exit  []int
}

// Event object
type Event struct {
	Name string
	Desc string
}

// Action object
type Action struct {
	Name string
	Desc string
}

// Rule object
type Rule struct {
	FromState int
	OnEvents  []int
	ToState   int
	Name      string
	Desc      string
}

// IsLoaded returns true if a formal specification was successfully loaded
func (ldr *Loader) IsLoaded() bool {
	return ldr.specLoaded
}

// String implements Stringer interface
func (ldr *Loader) String() string {
	out := ""
	out += fmt.Sprintf("Spec loaded: %v\n", ldr.specLoaded)
	out += fmt.Sprintf("last error: %v\n", ldr.lastError)

	if ldr.specLoaded {
		out += fmt.Sprintf("IR.Events:\n%v\n", ldr.IR.Events)
		out += fmt.Sprintf("IR.Actions:\n%v\n", ldr.IR.Actions)
		out += fmt.Sprintf("IR.States:\n%v\n", ldr.IR.States)
		out += fmt.Sprintf("IR.Rules:\n%v\n", ldr.IR.Rules)
	}

	return out
}
