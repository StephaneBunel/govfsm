// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package loader

import "errors"

// Errors eventually returned by loader package
var (
	ErrParsingSpecs  = errors.New("error parsing formal specifications")
	ErrSpecNotLoaded = errors.New("specification not loaded")
)
