// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package loader

import (
	"fmt"
	"sort"
	"strings"

	"gitlab.com/StephaneBunel/omap"

	yaml "gopkg.in/yaml.v2"
)

// LoadYAML parses and checks a YAML formal specifications
func (ldr *Loader) LoadYAML(yml string) error {
	specs := new(YamlVfsmSpec)

	ldr.IR.init()

	if err := yaml.Unmarshal([]byte(yml), specs); err != nil {
		ldr.lastError = fmt.Errorf("Error loading YAML specifications: %w", err)
		return ldr.lastError
	}

	{ // Events
		numEvents := len(specs.Vfsm.Events)
		if numEvents == 0 {
			return fmt.Errorf("Events section must be presents: %w", ErrParsingSpecs)
		}
		for _, event := range specs.Vfsm.Events {
			name := event.Name
			if ldr.IR.Events.Exists(name) {
				return fmt.Errorf("duplicate event %v: %w", name, ErrParsingSpecs)
			}
			ldr.IR.Events.Set(omap.NewItem(name, Event{Name: name, Desc: event.Desc}))
		}
	}

	{ // Actions
		if len(specs.Vfsm.Actions) == 0 {
			return fmt.Errorf("Actions section must be presents: %w", ErrParsingSpecs)
		}
		for _, action := range specs.Vfsm.Actions {
			name := action.Name
			if ldr.IR.Actions.Exists(name) {
				return fmt.Errorf("duplicate action %v: %w", name, ErrParsingSpecs)
			}
			ldr.IR.Actions.Set(omap.NewItem(name, Action{Name: name, Desc: action.Desc}))
		}
	}

	{ // States
		if len(specs.Vfsm.States) == 0 {
			return fmt.Errorf("States section must be presents: %w", ErrParsingSpecs)
		}
		for _, state := range specs.Vfsm.States {
			key := state.Name
			if ldr.IR.States.Exists(key) {
				return fmt.Errorf("duplicate state %v: %w", key, ErrParsingSpecs)
			}

			entries := make([]int, 0, len(state.Entry))
			for i, entry := range ldr.IR.Actions.GetIndexesByKeys(state.Entry...) {
				if entry == -1 {
					return fmt.Errorf("state entry: action %q does not exists: %w", state.Entry[i], ErrParsingSpecs)
				}
				entries = append(entries, entry)
			}

			exites := make([]int, 0, len(state.Exit))
			for i, exit := range ldr.IR.Actions.GetIndexesByKeys(state.Exit...) {
				if exit == -1 {
					return fmt.Errorf("state exit: action %q does not exists: %w", state.Exit[i], ErrParsingSpecs)
				}
				exites = append(exites, exit)
			}

			ldr.IR.States.Set(omap.NewItem(key, State{
				Name:  key,
				Desc:  state.Desc,
				Entry: entries,
				Exit:  exites,
			}))
		}
	}

	{ // Rules
		if len(specs.Vfsm.Rules) == 0 {
			return fmt.Errorf("Rules section must be presents: %w", ErrParsingSpecs)
		}
		for _, rule := range specs.Vfsm.Rules {
			// fromStateIndex
			fromStateIndex := ldr.IR.States.GetIndexesByKeys(rule.FromState)[0]
			if fromStateIndex == -1 {
				return fmt.Errorf("from_state %q not found but wanted in rule %+v: %w", rule.FromState, rule, ErrParsingSpecs)
			}

			// toStateIndex
			toStateIndex := ldr.IR.States.GetIndexesByKeys(rule.ToState)[0]
			if toStateIndex == -1 {
				return fmt.Errorf("to_state %q not found but wanted in rule %+v: %w", rule.ToState, rule, ErrParsingSpecs)
			}

			// whenEventMap
			whenEventMap := make(map[int]string, len(rule.OnEvents))
			for i, eventIndex := range ldr.IR.Events.GetIndexesByKeys(rule.OnEvents...) {
				eventName := rule.OnEvents[i]
				if eventIndex == -1 {
					return fmt.Errorf("event %q not found but wanted in rule %+v: %w", eventName, rule, ErrParsingSpecs)
				}
				whenEventMap[eventIndex] = eventName
			}

			eventIndexes := make([]int, 0, len(whenEventMap))
			for eventIndex := range whenEventMap {
				eventIndexes = append(eventIndexes, int(eventIndex))
			}
			sort.Ints(eventIndexes)

			eventsNames := make([]string, 0, len(whenEventMap))
			for _, index := range eventIndexes {
				eventsNames = append(eventsNames, fmt.Sprintf("%q", whenEventMap[index]))
			}

			ruleKey := fmt.Sprintf("IF state is %q AND inputs are ( %v ) THEN state become %q",
				rule.FromState, strings.Join(eventsNames, " & "), rule.ToState)

			vRule := Rule{
				Name:      rule.Name,
				Desc:      rule.Desc,
				FromState: int(fromStateIndex),
				OnEvents:  eventIndexes,
				ToState:   int(toStateIndex),
			}
			ldr.IR.Rules.Set(omap.NewItem(ruleKey, vRule))
		}
	}

	{ // Metadata (at the end)

		// Start state
		if specs.Vfsm.Metadata.StartState == "" {
			return fmt.Errorf("metadata.start_state is missing or empty: %w", ErrParsingSpecs)
		}
		stateIndex := ldr.IR.States.GetIndexesByKeys(specs.Vfsm.Metadata.StartState)[0]
		if stateIndex == -1 {
			return fmt.Errorf("metadata.start_state refer to an unknown state (%v): %w", specs.Vfsm.Metadata.StartState, ErrParsingSpecs)
		}
		ldr.IR.stateStart = stateIndex

		// Stop state
		if len(specs.Vfsm.Metadata.StopStates) == 0 {
			return fmt.Errorf("metadata.stop_states is missing or empty: %w", ErrParsingSpecs)
		}
		ldr.IR.stateStops = make(map[int]struct{}, len(specs.Vfsm.Metadata.StopStates))
		for i, stopIndex := range ldr.IR.States.GetIndexesByKeys(specs.Vfsm.Metadata.StopStates...) {
			if stopIndex == -1 {
				return fmt.Errorf("metadata.stop_states refer to an unknown state %q: %w", specs.Vfsm.Metadata.StopStates[i], ErrParsingSpecs)
			}
			ldr.IR.stateStops[stopIndex] = struct{}{}
		}

		// Limits
		ldr.IR.pathLimit = specs.Vfsm.Metadata.PathLimit
		ldr.IR.cyclesLimit = specs.Vfsm.Metadata.CycleLimit

		// Options
		if len(specs.Vfsm.Metadata.Options) != 0 {
			for _, option := range specs.Vfsm.Metadata.Options {
				opt := strings.TrimSpace(strings.ToLower(option))
				ldr.IR.Options.SetSilently(omap.NewItem(opt, omap.EmptyValue))
			}
		}
	}

	ldr.specLoaded = true

	return nil
}

type YamlVfsmSpec struct {
	Vfsm struct {
		Metadata struct {
			Name       string   `yaml:"name,omitempty"`
			Desc       string   `yaml:"desc,omitempty"`
			Version    string   `yaml:"version,omitempty"`
			StartState string   `yaml:"start_state"`
			StopStates []string `yaml:"stop_states"`
			PathLimit  int      `yaml:"path_limit,omitempty"`
			CycleLimit int      `yaml:"cycle_limit,omitempty"`
			Options    []string `yaml:"options,omitempty"`
		} `yaml:"metadata"`
		Events []struct {
			Name string `yaml:"name"`
			Desc string `yaml:"desc,omitempty"`
		} `yaml:"events"`
		Actions []struct {
			Name string `yaml:"name"`
			Desc string `yaml:"desc,omitempty"`
		} `yaml:"actions"`
		States []struct {
			Name  string   `yaml:"name"`
			Desc  string   `yaml:"desc,omitempty"`
			Exit  []string `yaml:"exit,omitempty"`
			Entry []string `yaml:"entry,omitempty"`
		} `yaml:"states"`
		Rules []struct {
			FromState string   `yaml:"from_state"`
			ToState   string   `yaml:"to_state"`
			OnEvents  []string `yaml:"on_events"`
			Name      string   `yaml:"name,omitempty"`
			Desc      string   `yaml:"desc,omitempty"`
		} `yaml:"rules"`
	} `yaml:"vfsm"`
}
