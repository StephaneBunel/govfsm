// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package loader_test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/StephaneBunel/vfsm/loader"
	"gitlab.com/StephaneBunel/vfsm/test"
)

func Test_FromYAML(t *testing.T) {
	ldr := loader.New()
	if err := ldr.LoadYAML(test.YamlFormalSpecTest); err != nil {
		t.Errorf("want no error, got %v", err)
		return
	}

	if ldr.IsLoaded() == false {
		t.Errorf("want %v, got %v", true, ldr.IsLoaded())
	}

	_ = fmt.Sprintln(ldr)
}

func Test_FromYAMLFail(t *testing.T) {
	ldr := loader.New()
	if err := ldr.LoadYAML("BAD"); err == nil {
		t.Errorf("want %v, got %v", loader.ErrParsingSpecs, err)
	}
}

func Test_FromYAMLNoEvents(t *testing.T) {
	ldr := loader.New()

	err := ldr.LoadYAML("---")
	wantS, gotS := "Events section must be presents", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLEventsDup(t *testing.T) {
	ldr := loader.New()

	err := ldr.LoadYAML(`---
    vfsm:
      events: [ { name: "ev1" }, { name: "ev1" } ]
        `)

	wantS, gotS := "duplicate event", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLNoActions(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }, ]
    `)

	wantS, gotS := "Actions section must be presents", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLActionDup(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" }, { name: "act1" } ]
    `)

	wantS, gotS := "duplicate action", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLNoState(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
    `)

	wantS, gotS := "States section must be presents", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLStateDup(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" }, { name: "state1" } ]
    `)

	wantS, gotS := "duplicate state", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLNoRules(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
    `)

	wantS, gotS := "Rules section must be presents", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLStateNoEntry(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1", entry: [ "unknown()" ] } ]
    `)

	wantS, gotS := "state entry", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLStateNoExit(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1", exit: [ "unknown()" ] } ]
    `)

	wantS, gotS := "state exit", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLRulesFromStateFail(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
  rules: [ { from_state: "unknown", to_state: "state1", on_events: [ "ev1" ] } ]
    `)

	wantS, gotS := "from_state", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLRulesToStateFail(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
  rules: [ { from_state: "state1", to_state: "unknown", on_events: [ "ev1" ] } ]
    `)

	wantS, gotS := "to_state", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLRulesOnEventFail(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
  rules: [ { from_state: "state1", to_state: "state1", on_events: [ "unknown" ] } ]
    `)

	wantS, gotS := "event ", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLMetadataStartStateFail(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
  rules: [ { from_state: "state1", to_state: "state1", on_events: [ "ev1" ] } ]
  metadata:
  `)

	wantS, gotS := "metadata.start_state is missing or empty", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLMetadataStartStateFail2(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
  rules: [ { from_state: "state1", to_state: "state1", on_events: [ "ev1" ] } ]
  metadata:
    start_state: unknown
  `)

	wantS, gotS := "metadata.start_state refer to an unknown state", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLMetadataStopState(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
  rules: [ { from_state: "state1", to_state: "state1", on_events: [ "ev1" ] } ]
  metadata:
    start_state: state1
  `)

	wantS, gotS := "metadata.stop_states is missing or empty", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}

func Test_FromYAMLMetadataStopState2(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(`---
vfsm:
  events: [ { name: "ev1" }]
  actions: [ { name: "act1" } ]
  states: [ { name: "state1" } ]
  rules: [ { from_state: "state1", to_state: "state1", on_events: [ "ev1" ] } ]
  metadata:
    start_state: state1
    stop_states: [ "unknown" ]
`)

	wantS, gotS := "metadata.stop_states refer to an unknown state", err.Error()
	if !strings.HasPrefix(gotS, wantS) {
		t.Errorf("want %v, got %v", wantS, gotS)
	}
}
