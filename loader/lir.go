// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package loader

import "gitlab.com/StephaneBunel/omap"

// LIR is the Loader Intermediate Representation of a future Machine
type LIR struct {
	Events      *omap.Omap
	Actions     *omap.Omap
	States      *omap.Omap
	Rules       *omap.Omap
	Options     *omap.Omap
	stateStart  int
	stateStops  map[int]struct{}
	cyclesLimit int
	pathLimit   int
}

func (lir *LIR) init() {
	lir.Events = omap.New(omap.OptionDefault)
	lir.Actions = omap.New(omap.OptionDefault)
	lir.Rules = omap.New(omap.OptionDefault)
	lir.States = omap.New(omap.OptionDefault).SetSilently(
		// First state (#0) must be the  always ("*") state
		omap.NewItem("*", State{Name: "*", Desc: "always check these rules"}))
	lir.Options = omap.New(omap.OptionDefault)
	lir.stateStops = make(map[int]struct{})
	lir.pathLimit = 1471
	lir.cyclesLimit = 10000
}
