// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package machine

import "errors"

// machine API errors
var (
	ErrCyclesLimit    = errors.New("machine reach the cycles limit")
	ErrPathsLimit     = errors.New("machine reach the path limit")
	ErrUnexpectedMode = errors.New("unexpected machine mode. This is a bug !")
)
