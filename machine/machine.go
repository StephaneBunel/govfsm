// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package machine

import (
	"fmt"

	"gitlab.com/StephaneBunel/bitfield"
)

// New returns an empty Machine object
func New() *Machine {
	m := new(Machine)
	m.init()
	return m
}

// Machine object
type Machine struct {
	StateToEntryMap map[int][]int
	StateToExistMap map[int][]int
	StateToRulesMap map[int][]Rule
	StateStopMap    map[int]struct{}
	StateStart      int
	PathLimit       int
	CycleLimit      int
	Options         int
	Status          struct {
		CurrentState int
		MachineMode  int
		Path         []int
		Cycle        int
		Error        error
	}
}

// Machine options
const (
	OptionsRecordPath = 1 << iota // Record all crossed states
	OptionsDebug      = 1 << iota // Print changes
)

// Machine internal mode
const (
	ModeUnknow = iota
	ModeInitialized
	ModeStart
	ModeRun
	ModeStopped
)

// Rule used by Machine
type Rule struct {
	ToState    int
	WhenEvents []int
}

func (m *Machine) init() {
	m.StateToEntryMap = make(map[int][]int)
	m.StateToExistMap = make(map[int][]int)
	m.StateToRulesMap = make(map[int][]Rule) // NB: State #0 is the always state
	m.StateStopMap = make(map[int]struct{})
	m.PathLimit = 128
	m.CycleLimit = 1024
	m.Status.Path = make([]int, 0)
	m.Status.MachineMode = ModeInitialized
}

// Reset resets the state of the machine to it's beginning
func (m *Machine) Reset() {
	switch m.Status.MachineMode {
	case ModeUnknow:
		return
	default:
		m.Status.CurrentState = m.StateStart
		m.Status.Path = make([]int, 0)
		m.Status.Cycle = 0
		m.Status.Error = nil
		m.Status.MachineMode = ModeStart
	}
}

// Error returns the last error or nil
func (m *Machine) Error() error {
	return m.Status.Error
}

// Running returns false if the Machine is stooped, else true
func (m *Machine) Running() bool {
	return m.Status.MachineMode != ModeStopped
}

// Run runs the machine according to the given events
func (m *Machine) Run(bf bitfield.Bitfielder) []int {
	result := make([]int, 0, 16)

	if m.Running() == false {
		return result
	}

	if len(m.Status.Path) >= m.PathLimit {
		m.Status.MachineMode = ModeStopped
		m.Status.Error = ErrPathsLimit
		return result
	}

	if m.Status.Cycle >= m.CycleLimit {
		m.Status.MachineMode = ModeStopped
		m.Status.Error = ErrCyclesLimit
		return result
	}
	m.Status.Cycle++

	if m.Options&OptionsDebug != 0 {
		fmt.Printf("* state = %v, mode = %v\n", m.Status.CurrentState, m.Status.MachineMode)
	}

	switch m.Status.MachineMode {

	case ModeStart:
		if m.Options|OptionsRecordPath != 0 {
			// Add new state in path
			m.Status.Path = append(m.Status.Path, m.Status.CurrentState)
		}

		// Result is entry action
		result = append(result, m.StateToEntryMap[m.Status.CurrentState]...)

		// Is current state a stop state ?
		if _, in := m.StateStopMap[m.Status.CurrentState]; in {
			// Yes! add exit action to result
			result = append(result, m.StateToExistMap[m.Status.CurrentState]...)
			if m.Options&OptionsDebug != 0 {
				fmt.Printf("	state is a STOP state. entry + exit = %v\n", result)
			}
			m.Status.MachineMode = ModeStopped
			if m.Options&OptionsDebug != 0 {
				fmt.Printf("	switching mode to = %v\n", m.Status.MachineMode)
			}
		} else {
			m.Status.MachineMode = ModeRun // Wait until a rule trigger
			if m.Options&OptionsDebug != 0 {
				fmt.Printf("	switching mode to = %v\n", m.Status.MachineMode)
			}
		}

	case ModeRun:
		rule := m.checkRules(bf)
		if rule == nil {
			break
		}

		// result is the exit action of the current state
		result = append(result, m.StateToExistMap[m.Status.CurrentState]...)
		if m.Options&OptionsDebug != 0 {
			fmt.Printf("	exit = %v\n", result)
		}

		// Switch current state the new state
		m.Status.CurrentState = rule.ToState
		if m.Options&OptionsDebug != 0 {
			fmt.Printf("	switching state to = %v\n", m.Status.CurrentState)
		}

		// append entry action of the new state
		result = append(result, m.StateToEntryMap[m.Status.CurrentState]...)
		if m.Options&OptionsDebug != 0 {
			fmt.Printf("	entry = %v\n", result)
		}

		if m.Options|OptionsRecordPath != 0 {
			// Add new state in path
			m.Status.Path = append(m.Status.Path, m.Status.CurrentState)
		}

		// Is new current state a stop state ?
		if _, in := m.StateStopMap[m.Status.CurrentState]; in {
			// Yes! append exit action
			result = append(result, m.StateToExistMap[m.Status.CurrentState]...)
			if m.Options&OptionsDebug != 0 {
				fmt.Printf("	state is a STOP state. entry + exit = %v\n", result)
			}
			// Stop the machine
			m.Status.MachineMode = ModeStopped
		}

	default:
		// We should never goes here
		m.Status.Error = ErrUnexpectedMode
		m.Status.MachineMode = ModeStopped

	}

	if m.Options&OptionsDebug != 0 {
		fmt.Printf("	return actions = %v\n", result)
	}

	return result
}

func (m *Machine) checkRules(bf bitfield.Bitfielder) *Rule {
	var ptrRule *Rule = nil

	if m.Options&OptionsDebug != 0 {
		fmt.Printf("	checkRules():\n")
		fmt.Printf("		events bit field = %v\n", bf)
	}

loop:
	for _, state := range []int{0, m.Status.CurrentState} {
		for _, rule := range m.StateToRulesMap[state] {
			if m.Options&OptionsDebug != 0 {
				fmt.Printf("		checkRules(%v)\n", rule.WhenEvents)
			}
			if bf.Test(rule.WhenEvents...) {
				ptrRule = &rule
				break loop
			}
		}
	}

	if ptrRule != nil && m.Options&OptionsDebug != 0 {
		fmt.Printf("		triggered: %v\n", *ptrRule)
	}

	return ptrRule
}
