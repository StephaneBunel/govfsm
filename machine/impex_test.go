// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package machine_test

import (
	"fmt"
	"testing"

	"gitlab.com/StephaneBunel/vfsm/loader"
	"gitlab.com/StephaneBunel/vfsm/machine"
	"gitlab.com/StephaneBunel/vfsm/test"
)

func Test_ImportBad(t *testing.T) {
	m := machine.New()
	m, err := m.ImportJSON([]byte("BAD"))
	if err == nil {
		t.Errorf("want %v, got %v", "error", nil)
	}
}

func Test_Import(t *testing.T) {
	m := machine.New()
	m, err := m.ImportJSON([]byte(test.JsonMachineSpecTest))
	if err != nil {
		t.Errorf("want %v, got %v", nil, err)
	}
}

func Test_Export(t *testing.T) {
	ldr := loader.New()
	err := ldr.LoadYAML(test.YamlFormalSpecTest)
	if err != nil {
		t.Errorf("want %v, got %v", nil, err)
		return
	}

	m, err := ldr.ToMachine()
	if err != nil {
		t.Errorf("want %v, got %v", nil, err)
		return
	}

	json, err := m.ExportJSON()
	fmt.Printf("err = %v\n, JSON = %s\n", err, json)
}
