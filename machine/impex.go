// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package machine

import "encoding/json"

// ExportJSON returns a full Machine state as a JSON
// A Machine can be exported at any time.
// You can export a machine, store it somewhere and reload it when needed.
func (m *Machine) ExportJSON() ([]byte, error) {
	return json.Marshal(m)
}

// ImportJSON imports a full machine state from JSON
// After a successful import you must use the returned Machine
func (m *Machine) ImportJSON(text []byte) (*Machine, error) {
	newMachine := New()
	err := json.Unmarshal(text, &newMachine)
	if err != nil {
		return nil, err
	}
	return newMachine, nil
}
