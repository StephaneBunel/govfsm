// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

package machine_test

import (
	"testing"

	"gitlab.com/StephaneBunel/bitfield"
	"gitlab.com/StephaneBunel/vfsm/machine"
	"gitlab.com/StephaneBunel/vfsm/test"
)

func Test_New(t *testing.T) {
	m := machine.New()

	if m.StateToEntryMap == nil {
		t.Errorf("want %v, got %v", "!nil", nil)
	}
	if m.StateToExistMap == nil {
		t.Errorf("want %v, got %v", "!nil", nil)
	}
	if m.StateToRulesMap == nil {
		t.Errorf("want %v, got %v", "!nil", nil)
	}
	if m.StateStopMap == nil {
		t.Errorf("want %v, got %v", "!nil", nil)
	}
	if m.Status.Path == nil {
		t.Errorf("want %v, got %v", "!nil", nil)
	}
	if m.Status.MachineMode != machine.ModeInitialized {
		t.Errorf("want %v, got %v", machine.ModeInitialized, m.Status.MachineMode)
	}
	if m.PathLimit < 0 {
		t.Errorf("want %v, got %v", ">= 0", m.PathLimit)
	}
	if m.CycleLimit < 0 {
		t.Errorf("want %v, got %v", ">= 0", m.CycleLimit)
	}
	if m.Options != 0 {
		t.Errorf("want %v, got %v", 0, m.Options)
	}
	if m.Error() != nil {
		t.Errorf("want %v, got %v", nil, m.Error())
	}
}

func Test_NewReset(t *testing.T) {
	m := machine.New()
	m.Status.CurrentState = 666

	m.Reset()
	if m.Status.CurrentState == 666 {
		t.Errorf("want %v, got %v", m.StateStart, m.Status.CurrentState)
	}

	m.Status.MachineMode = machine.ModeUnknow
	m.Reset()
	if m.Status.MachineMode != machine.ModeUnknow {
		t.Errorf("want %v, got %v", machine.ModeUnknow, m.Status.MachineMode)
	}
}

func Test_UnexpectedState(t *testing.T) {
	m := machine.New()
	e := bitfield.New(0, bitfield.OptAutoExpand)
	m.Reset()
	m.Status.MachineMode = 666
	m.Run(e)
	err := m.Error()
	if err != machine.ErrUnexpectedMode {
		t.Errorf("want %v, got %v", machine.ErrUnexpectedMode, err)
	}
	if m.Status.MachineMode != machine.ModeStopped {
		t.Errorf("want %v, got %v", machine.ModeStopped, m.Status.MachineMode)
	}
}

type Step struct {
	beforeState int   // before run
	beforeMode  int   // before run
	actions     []int // after run
	afterState  int   // after run
	afterMode   int   // after run
	events      []int // event set after run
}

func RunMachineStepByStep(t *testing.T, m *machine.Machine, events bitfield.Bitfielder, steps []Step) {
	for n, step := range steps {
		if step.beforeState != m.Status.CurrentState {
			t.Errorf("@%d, beforeState: want %v, got %v", n, step.beforeState, m.Status.CurrentState)
			return
		}
		if step.beforeMode != m.Status.MachineMode {
			t.Errorf("@%d, beforeMode: want %v, got %v", n, step.beforeMode, m.Status.MachineMode)
			return
		}
		actions := m.Run(events)
		if len(step.actions) != len(actions) {
			t.Errorf("@%d, len(actions): want %v, got %v", n, len(step.actions), len(actions))
			return
		}
		for i, action := range step.actions {
			if action != actions[i] {
				t.Errorf("@%d, actions[%d]: want %v, got %v", n, i, action, actions[i])
				return
			}
		}
		if step.afterState != m.Status.CurrentState {
			t.Errorf("@%d, afterState: want %v, got %v", n, step.afterState, m.Status.CurrentState)
			return
		}
		if step.afterMode != m.Status.MachineMode {
			t.Errorf("@%d, afterMode: want %v, got %v", n, step.afterMode, m.Status.MachineMode)
			return
		}
		events.Set(step.events...)
	}
}

func Test_RunFull(t *testing.T) {
	var err error
	var events = bitfield.New(15, bitfield.OptDefault)
	var m = machine.New()

	if m, err = m.ImportJSON([]byte(test.JsonMachineSpecTest)); err != nil {
		t.Errorf("want %v, got %v", nil, err)
		return
	}

	var steps = []Step{
		{3, machine.ModeStart,
			[]int{1}, 3, machine.ModeRun, // actions = entry (1:params.validate())
			[]int{}},
		{3, machine.ModeRun,
			[]int{}, 3, machine.ModeRun,
			[]int{1}}, // add event new_req
		{3, machine.ModeRun,
			[]int{}, 3, machine.ModeRun,
			[]int{2}}, // event params_ok
		{3, machine.ModeRun,
			[]int{0, 0, 2}, 4, machine.ModeRun, // actions = exit (0) + entry (0,2)
			[]int{}},
		{4, machine.ModeRun,
			[]int{}, 4, machine.ModeRun,
			[]int{4}}, // event image loaded
		{4, machine.ModeRun,
			[]int{0, 0, 4}, 6, machine.ModeRun, // action = exit (0) + entry (0,4)
			[]int{9}}, // event resize_ok
		{6, machine.ModeRun,
			[]int{0, 0, 5}, 7, machine.ModeRun, // action = exit (0) + entry (0,5)
			[]int{10}}, // event flush_ok
		{7, machine.ModeRun,
			[]int{0, 0}, 1, machine.ModeStopped, // action = exit (0) + entry () + exit (0)
			[]int{}},
		{1, machine.ModeStopped,
			[]int{}, 1, machine.ModeStopped, // action = ()
			[]int{}},
	}

	m.Reset()
	m.Options |= machine.OptionsRecordPath | machine.OptionsDebug
	RunMachineStepByStep(t, m, events, steps)

	if m.Running() == true {
		t.Errorf("want %v, got %v", false, m.Running())
	}

	if len(m.Status.Path) != 5 {
		t.Errorf("want %v, got %v", 1, len(m.Status.Path))
	}

	for i, p := range []int{3, 4, 6, 7, 1} {
		if m.Status.Path[i] != p {
			t.Errorf("want %v, got %v", p, m.Status.Path[i])
		}
	}

}

func Test_RunStop(t *testing.T) {
	var err error
	var events = bitfield.New(15, bitfield.OptDefault)
	var m = machine.New()

	if m, err = m.ImportJSON([]byte(test.JsonMachineSpecTest)); err != nil {
		t.Errorf("want %v, got %v", nil, err)
		return
	}

	m.Reset()
	m.Options |= machine.OptionsRecordPath | machine.OptionsDebug
	m.Status.CurrentState = 1 // STOP
	RunMachineStepByStep(t, m, events,
		[]Step{
			{1, machine.ModeStart,
				[]int{0}, 1, machine.ModeStopped, // actions = entry () + exit (0)
				[]int{}},
		},
	)

	if len(m.Status.Path) != 1 {
		t.Errorf("want %v, got %v", 1, len(m.Status.Path))
	}
}

func Test_RunCycleLimit(t *testing.T) {
	var err error
	var events = bitfield.New(15, bitfield.OptDefault)
	var m = machine.New()

	if m, err = m.ImportJSON([]byte(test.JsonMachineSpecTest)); err != nil {
		t.Errorf("want %v, got %v", nil, err)
		return
	}

	m.Reset()
	m.Options |= machine.OptionsRecordPath
	m.CycleLimit = 10
	for m.Running() { // Loop until cycle limit
		m.Run(events)
	}

	err = m.Error()
	if err != machine.ErrCyclesLimit {
		t.Errorf("want %v, got %v", machine.ErrCyclesLimit, err)
	}

	if m.Status.Cycle != m.CycleLimit {
		t.Errorf("want %v, got %v", m.CycleLimit, m.Status.Cycle)
	}

}

func Test_RunPathLimit(t *testing.T) {
	var err error
	var events = bitfield.New(15, bitfield.OptDefault)
	var m = machine.New()

	if m, err = m.ImportJSON([]byte(test.JsonMachineSpecTest)); err != nil {
		t.Errorf("want %v, got %v", nil, err)
		return
	}

	m.Reset()
	m.PathLimit = 2
	m.Options |= machine.OptionsRecordPath

	for _, step := range []Step{
		{3, machine.ModeStart,
			[]int{1}, 3, machine.ModeRun, // actions = entry (1:params.validate())
			[]int{}},
		{3, machine.ModeRun,
			[]int{}, 3, machine.ModeRun,
			[]int{1}}, // add event new_req
		{3, machine.ModeRun,
			[]int{}, 3, machine.ModeRun,
			[]int{2}}, // event params_ok
		{3, machine.ModeRun,
			[]int{0, 0, 2}, 4, machine.ModeRun, // actions = exit (0) + entry (0,2)
			[]int{}},
		{4, machine.ModeRun,
			[]int{}, 4, machine.ModeRun,
			[]int{4}}, // event image loaded
		{4, machine.ModeRun,
			[]int{0, 0, 4}, 6, machine.ModeRun, // action = exit (0) + entry (0,4)
			[]int{9}}, // event resize_ok
		{6, machine.ModeRun,
			[]int{0, 0, 5}, 7, machine.ModeRun, // action = exit (0) + entry (0,5)
			[]int{10}}, // event flush_ok

	} {
		m.Run(events)
		events.Set(step.events...)
	}

	err = m.Error()
	if err != machine.ErrPathsLimit {
		t.Errorf("want %v, got %v", machine.ErrPathsLimit, err)
	}

	if len(m.Status.Path) != m.PathLimit {
		t.Errorf("want %v, got %v (%v)", m.PathLimit, len(m.Status.Path), m.Status.Path)
	}

}
