module gitlab.com/StephaneBunel/vfsm

go 1.14

require (
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/nwillc/gorelease v1.1.1 // indirect
	github.com/olekukonko/tablewriter v0.0.4
	gitlab.com/StephaneBunel/bitfield v0.0.0-20200830215557-30b36947b8a3
	gitlab.com/StephaneBunel/omap v0.9.2
	gopkg.in/yaml.v2 v2.3.0
)
