package vfsm

import (
	"strconv"
	"strings"

	"github.com/olekukonko/tablewriter"
	"gitlab.com/StephaneBunel/vfsm/loader"
)

// String returns a representation of loaded specifications
func (v *Vfsm) String() (out string) {
	ldr := v.ldr
	if ldr.IsLoaded() == false {
		out += "Please load a Vfsm formal specification\n"
		return
	}

	{ // event
		data := make([][]string, 0)
		for iter := ldr.IR.Events.IterItems(); iter.Next(); {
			index, eventItem := iter.Item()
			event := eventItem.Value().(loader.Event)
			data = append(data, []string{strconv.Itoa(index), event.Name, event.Desc})
		}

		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)
		table.SetHeader([]string{"ID", "Event Name", "Description"})
		table.SetCaption(true, "Virtual event")
		table.SetAutoWrapText(false)
		table.AppendBulk(data)
		table.Render()
		out += tableString.String() + "\n\n"
	}

	{ // action
		data := make([][]string, 0)
		for iter := ldr.IR.Actions.IterItems(); iter.Next(); {
			index, actionItem := iter.Item()
			action := actionItem.Value().(loader.Action)
			data = append(data, []string{strconv.Itoa(index), action.Name, action.Desc})
		}

		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)
		table.SetHeader([]string{"ID", "Action Name", "Description"})
		table.SetCaption(true, "Virtual action")
		table.SetAutoWrapText(false)
		table.AppendBulk(data)
		table.Render()
		out += tableString.String() + "\n\n"
	}

	{ // states
		data := make([][]string, 0)

		for iter := ldr.IR.States.IterItems(); iter.Next(); {
			index, stateItem := iter.Item()
			state := stateItem.Value().(loader.State)
			entries := strings.Join(ldr.IR.Actions.GetKeysByIndexes(state.Entry...), ",")
			exites := strings.Join(ldr.IR.Actions.GetKeysByIndexes(state.Exit...), ",")
			data = append(data, []string{strconv.Itoa(index), state.Name, entries, exites, state.Desc})
		}

		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)
		table.SetHeader([]string{"ID", "State Name", "Entry", "Exit", "Description"})
		table.SetCaption(true, "States")
		table.SetAutoWrapText(false)
		table.AppendBulk(data)
		table.Render()
		out += tableString.String() + "\n\n"
	}

	{ // Rules
		data := make([][]string, 0)
		for iter := ldr.IR.Rules.IterItems(); iter.Next(); {
			_, ruleItem := iter.Item()
			rule := ruleItem.Value().(loader.Rule)
			fromState := ldr.IR.States.GetKeysByIndexes(rule.FromState)[0]
			toState := ldr.IR.States.GetKeysByIndexes(rule.ToState)[0]
			onEvents := strings.Join(ldr.IR.Events.GetKeysByIndexes(rule.OnEvents...), " & ")
			data = append(data, []string{fromState, toState, onEvents, rule.Name, rule.Desc})
		}

		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)
		table.SetHeader([]string{"From state", "To state", "On Events", "Name", "Description"})
		table.SetCaption(true, "Rules")
		table.SetAutoWrapText(false)
		table.AppendBulk(data)
		table.Render()
		out += tableString.String() + "\n\n"
	}

	return
}
