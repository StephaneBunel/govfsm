# goVFSM

A simple Virtual Finite State Machine for Go.

# Documentations

module [vfsm](doc.md)

package [loader](loader/doc.md)

package [machine](machine/doc.md)

package [test](test/doc.md)

## A simple Vfsm

This graph is produced from the simple Vfsm used by unit tests

![graph](digraph/digraph.svg)
