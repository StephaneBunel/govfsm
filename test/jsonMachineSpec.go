package test

/*
IR.Events:
0:error
1:new_req
2:params_ok
3:params_bad
4:image_loaded
5:image_load_failed
6:image_resized
7:image_flushed
8:max_reload
9:resize_ok
10:flush_ok

IR.Actions:
0:log_timestamp()
1:params.validate()
2:image.load()
3:image.reload()
4:image.resize()
5:image.send()
6:fail()

IR.States:
0:*
1:STOP
2:FAILED
3:START
4:LOAD
5:RELOAD
6:RESIZE
7:FLUSH

IR.Rules:
0:WHEN state is "*" AND inputs are ( "error" ) THEN state become "FAILED"
1:WHEN state is "START" AND inputs are ( "new_req" & "params_ok" ) THEN state become "LOAD"
2:WHEN state is "LOAD" AND inputs are ( "params_ok" & "image_loaded" ) THEN state become "RESIZE"
3:WHEN state is "LOAD" AND inputs are ( "image_load_failed" ) THEN state become "RELOAD"
4:WHEN state is "RELOAD" AND inputs are ( "image_loaded" ) THEN state become "RESIZE"
5:WHEN state is "RELOAD" AND inputs are ( "max_reload" ) THEN state become "FAILED"
6:WHEN state is "RELOAD" AND inputs are ( "image_load_failed" ) THEN state become "RELOAD"
7:WHEN state is "RESIZE" AND inputs are ( "resize_ok" ) THEN state become "FLUSH"
8:WHEN state is "FLUSH" AND inputs are ( "flush_ok" ) THEN state become "STOP"

*/

var JsonMachineSpecTest = []byte(`{"StateToEntryMap":{"0":[],"1":[],"2":[6],"3":[1],"4":[0,2],"5":[0,3],"6":[0,4],"7":[0,5]},"StateToExistMap":{"0":[],"1":[0],"2":[0],"3":[0],"4":[0],"5":[0],"6":[0],"7":[0]},"StateToRulesMap":{"0":[{"ToState":2,"WhenEvents":[0]}],"3":[{"ToState":4,"WhenEvents":[1,2]}],"4":[{"ToState":6,"WhenEvents":[2,4]},{"ToState":5,"WhenEvents":[5]}],"5":[{"ToState":6,"WhenEvents":[4]},{"ToState":2,"WhenEvents":[8]},{"ToState":5,"WhenEvents":[5]}],"6":[{"ToState":7,"WhenEvents":[9]}],"7":[{"ToState":1,"WhenEvents":[10]}]},"StateStopMap":{"1":{},"2":{}},"StateStart":3,"PathLimit":10,"CycleLimit":20,"Options":1,"Status":{"CurrentState":0,"MachineMode":1,"Path":[],"Cycle":0,"Error":null}}`)
