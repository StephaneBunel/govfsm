package test

const YamlFormalSpecTest = `
---
vfsm:
  metadata:
    name: "Image resizer"
    version: "1"
    start_state: "START"
    stop_states: [ "STOP", "FAILED" ]
    path_limit: 10
    cycle_limit: 20
    options: [ "--record-path" ]

  events:
    - { name: "error", desc: "global error" }
    - name: "new_req"
    - name: "params_ok"
    - name: "params_bad"
    - name: "image_loaded"
    - name: "image_load_failed"
    - name: "image_resized"
    - name: "image_flushed"
    - name: "max_reload"
    - name: "resize_ok"
    - name: "flush_ok"

  actions:
    - { name: "log_timestamp()", desc: "Trace the date and time of the event" }
    - name: "params.validate()"
    - name: "image.load()"
    - { name: "image.reload()", desc: "Use a backoff reload" }
    - name: "image.resize()"
    - name: "image.send()"
    - name: "fail()"

  states:
    # NB: state #0 is the always "*" state
    - { name: "STOP",    entry: [],                                       exit: [ "log_timestamp()" ], desc: "machine terminated" }
    - { name: "FAILED",  entry: [ "fail()" ],                             exit: [ "log_timestamp()" ], desc: "machine failed" }
    - { name: "START",   entry: [ "params.validate()" ],                  exit: [ "log_timestamp()" ] }
    - { name: "LOAD",    entry: [ "log_timestamp()", "image.load()" ],    exit: [ "log_timestamp()" ] }
    - { name: "RELOAD",  entry: [ "log_timestamp()", "image.reload()" ],  exit: [ "log_timestamp()" ] }
    - { name: "RESIZE",  entry: [ "log_timestamp()", "image.resize()" ],  exit: [ "log_timestamp()" ] }
    - { name: "FLUSH",   entry: [ "log_timestamp()", "image.send()" ],    exit: [ "log_timestamp()" ] }

  # - NB: Order matters. It will be the same during the VFSM execution
  # from_state: "*" is special. All rules under state "*" will be always evaluated before any others and whatever the current state is.
  rules:
    - from_state: "*"
      to_state: "FAILED"
      on_events: [ "error" ]
      name: "fail on error"
      desc: "state * means whatever the current state is"
    
    - from_state: "START"
      to_state: "LOAD"
      on_events: [ "params_ok", "new_req", "new_req" ] # !! duplicated event will be gently and silently ignored
      desc: "if parameters is valid then load image"

    - { from_state: "LOAD",    to_state: "RESIZE", on_events: [ "params_ok", "image_loaded" ] }

    # !! Duplicated rule will be gently and silently ignored !
    - { from_state: "LOAD",    to_state: "RESIZE",  on_events: [ "image_loaded", "params_ok" ] }
    - { from_state: "LOAD",    to_state: "RELOAD",  on_events: [ "image_load_failed" ] }
    - { from_state: "RELOAD",  to_state: "RESIZE",  on_events: [ "image_loaded" ] }
    - { from_state: "RELOAD",  to_state: "FAILED",  on_events: [ "max_reload" ] }
    - { from_state: "RELOAD",  to_state: "RELOAD",  on_events: [ "image_load_failed" ] }
    - { from_state: "RESIZE",  to_state: "FLUSH",   on_events: [ "resize_ok" ] }
    - { from_state: "FLUSH",   to_state: "STOP",    on_events: [ "flush_ok" ] }
`
