package vfsm

import (
	"fmt"
	"strings"

	"gitlab.com/StephaneBunel/bitfield"
	"gitlab.com/StephaneBunel/vfsm/loader"
	"gitlab.com/StephaneBunel/vfsm/machine"
)

// New creates a new VFSM
func New() *Vfsm {
	v := new(Vfsm)
	v.machine = machine.New()
	return v
}

// Vfsm main object
type Vfsm struct {
	machine *machine.Machine
	ldr     *loader.Loader
}

// LoadSpec load Vfsm specification according to format
// Available format are: yaml,
func (v *Vfsm) LoadSpec(format string, data string) error {
	var err error
	v.ldr = loader.New()

	switch f := strings.ToLower(format); f {
	case "yml", "yaml":
		if err = v.ldr.LoadYAML(data); err != nil {
			return err
		}
		v.machine, err = v.ldr.ToMachine()
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Unknown format: %q", f)
	}
	return nil
}

// SaveMachine returns a photo of the current machine for external storage
func (v *Vfsm) SaveMachine() ([]byte, error) {
	return v.machine.ExportJSON()
}

// LoadMachine loads a pre-saved photo of the machine to continue to run it
func (v *Vfsm) LoadMachine(data []byte) error {
	m, err := v.machine.ImportJSON(data)
	if err != nil {
		return err
	}
	v.machine = m
	return nil
}

// Next returns false when the Vfsm is stopped, else true
func (v *Vfsm) Next() bool {
	return v.machine.Running()
}

// Run runs the machine according to the given events
func (v *Vfsm) Run(events bitfield.Bitfielder) []int {
	return v.machine.Run(events)
}

// Reset resets the machine to it's initial state
func (v *Vfsm) Reset() {
	v.machine.Reset()
}

// Path returns the a list of crossed path
// The Vfsm must have been created with the Options: "--record-path"
func (v *Vfsm) Path() []int {
	return v.machine.Status.Path
}
