package vfsm_test

import (
	"testing"

	"gitlab.com/StephaneBunel/vfsm"
	"gitlab.com/StephaneBunel/vfsm/test"
)

func Test_LoadSpecs(t *testing.T) {
	v := vfsm.New()
	if err := v.LoadSpec("yaml", test.YamlFormalSpecTest); err != nil {
		t.Errorf("want %v, got %v", nil, err)
	}
}
