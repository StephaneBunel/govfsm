package vfsm_test

import (
	"strings"
	"testing"

	"gitlab.com/StephaneBunel/vfsm"
	"gitlab.com/StephaneBunel/vfsm/test"
)

func Test_String(t *testing.T) {
	v := vfsm.New()
	if err := v.LoadSpec("yaml", test.YamlFormalSpecTest); err != nil {
		t.Errorf("want %v, got %v", nil, err)
	}

	gotS := v.String()
	wantS := "4 | LOAD"
	if strings.Index(gotS, wantS) == -1 {
		t.Errorf("want: %v in string, got %v", wantS, gotS)
	}
}
